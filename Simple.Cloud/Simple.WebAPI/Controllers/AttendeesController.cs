﻿using System.Collections.Generic;
using System.Web.Http;
using DinguBlue.MeetingApp.Common.Interfaces;
using Simple.WebAPI.Models;
using Simple.WebAPI.Models.Repositories;


namespace Simple.WebAPI.Controllers
{
    public class AttendeesController : ApiController
    {

        private readonly IAttendeeRepository Repository;


         public AttendeesController(IAttendeeRepository attendeeRepository)
         {
             Repository = attendeeRepository;
         }

        public AttendeesController()
        {
            
        }

        [HttpGet]
        [Queryable]
        public IEnumerable<Attendee> Get()
        {
            return Repository.GetAll();
        }

        [HttpGet]
        public Attendee GetById(int id)
        {
            var result = Repository.GetById(id);

            return result;
        }

        [HttpGet]
        public IEnumerable<Attendee> GetByMeetingId(int meetingId)
        {
            return Repository.GetByMeetingId(meetingId);
        }

        [HttpPost]
        public void PostAttendee([FromBody]Attendee meeting)
        {
            Repository.Create(meeting);
        }

        [HttpPut]
        public void PutAttendee([FromBody]Attendee meeting)
        {
            Repository.Update(meeting);
        }

        [HttpDelete]
        public void DeleteAttendee(int id)
        {
            Repository.Delete(id);
        }

    }
}