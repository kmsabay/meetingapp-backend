﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DinguBlue.MeetingApp.Common.Enums;
using DinguBlue.MeetingApp.Common.Interfaces;
using Simple.WebAPI.Models;
using Simple.WebAPI.Models.Repositories;


namespace Simple.WebAPI.Controllers
{
    public class MeetingsController: BaseApiController 
    {
        private readonly IMeetingRepository _meetingRepository;
        
      
        public MeetingsController
            (IMeetingRepository meetingRepository, INotificationService n): base(n)
        {
            _meetingRepository = meetingRepository;
        }


        [Queryable] //OData support
        public IQueryable<Meeting> Get()
        {
            var result = _meetingRepository.GetAll();



            LoggingManager.Log(logMessage: "Test Logging");

            //LoggingManager.Log("sId100Test", NotifyAction.Refresh, "Meeting,Attendies", "Create new meeting", PriorityLevel.High);

            return result.AsQueryable();
        }

        public Meeting GetById(int id)
        {
            var result = _meetingRepository.GetById(id);

            return result;
        }

        public HttpResponseMessage PostMeeting([FromBody]Meeting meeting)
        {
            if (_meetingRepository.Create(meeting) == 1)
            {
                LoggingManager.Log(meeting.Id.ToString(), NotifyAction.Refresh, "Meeting,Attendies", "Create new meeting", PriorityLevel.High);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }

            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        public HttpResponseMessage PutMeeting([FromBody]Meeting meeting)
        {
            if (_meetingRepository.Update(meeting) == 1)
            {
                LoggingManager.Log(meeting.Id.ToString(), NotifyAction.Refresh, "Meeting,Attendies", "Updated meeting:" + meeting.Id, PriorityLevel.High);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }

            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        public HttpResponseMessage DeleteMeeting(int id)
        {
            if (_meetingRepository.Delete(id) == 1)
            {
                LoggingManager.Log(id.ToString(), NotifyAction.Refresh, "Meeting,Attendies", "Deleted meeting:" + id, PriorityLevel.High);
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

    }
}