﻿using System;
using System.Dynamic;
using System.Web.Http;
using DinguBlue.MeetingApp.Common.Enums;
using DinguBlue.MeetingApp.Common.Events;
using DinguBlue.MeetingApp.Common.Interfaces;
using Simple.WebAPI.Managers;
using Action = DinguBlue.MeetingApp.Common.Enums.NotifyAction;

namespace Simple.WebAPI.Controllers
{
    public class BaseApiController : ApiController
    {
        protected LoggingManager LoggingManager;

        public BaseApiController(INotificationService notificationService)
        {
            LoggingManager = new LoggingManager();

            // subscribe to notification service
            LoggingManager.NotificationTriggered += (model, args) => notificationService.Queue(args);

            // subscribe to loging service
            LoggingManager.LoggingTriggered += (model, args) => CallLogger(args);
        }


        // to do, global logging
        private void CallLogger(dynamic data)
        {
            //todo call Logging Service
            var s = data.Item.Message;
        }


     

       
    }
}