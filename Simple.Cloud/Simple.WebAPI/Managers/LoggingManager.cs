﻿using System;
using System.Dynamic;
using DinguBlue.MeetingApp.Common.Enums;
using DinguBlue.MeetingApp.Common.Events;

namespace Simple.WebAPI.Managers
{

    /// <summary>
    /// Handles logging and notification
    /// todo: to be refactored
    /// </summary>
    public class LoggingManager
    {
        public event OperationTriggeredHandler NotificationTriggered;
        public event OperationTriggeredHandler LoggingTriggered;

        public void OnOperationTriggered(object model, OperationTriggerEventArgs e)
        {
            if (NotificationTriggered != null)
            {
                NotificationTriggered(this, e);
            }
        }

        public void OnLoggingTriggered(object model, OperationTriggerEventArgs e)
        {
            OperationTriggeredHandler handler = LoggingTriggered;
            if (handler != null) handler(model, e);
        }


        public void Log(string senderId = null,
                          NotifyAction action = NotifyAction.Refresh,
                          string entities = "",
                          string message = "",
                          PriorityLevel priority = PriorityLevel.Low,
                          string logMessage = "")
        {
            dynamic operationMessage = new ExpandoObject();
            operationMessage.Log = new ExpandoObject();
            operationMessage.Notify = new ExpandoObject();

            if (senderId != null) // notification
            {
                operationMessage.Notify.SenderId = senderId;
                operationMessage.Notify.Message = message;
                operationMessage.Notify.Action = action;
                operationMessage.Notify.Entities = entities;
                operationMessage.Notify.PriorityLevel = priority;

                if (logMessage == string.Empty)
                {
                    logMessage = message;
                }

                OnOperationTriggered(this, new OperationTriggerEventArgs(operationMessage.Notify));
            }

            if (logMessage != string.Empty) // loging
            {
                operationMessage.Log.Message = DateTime.Now + " : " + logMessage;
                OnLoggingTriggered(this, new OperationTriggerEventArgs(operationMessage.Log));
            }

            
        } 
    }
}