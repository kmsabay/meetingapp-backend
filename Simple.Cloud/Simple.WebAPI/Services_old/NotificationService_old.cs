﻿using System;
using System.Collections.Generic;
using System.Web;
using DinguBlue.MeetingApp.Notification.Data;
using DinguBlue.MeetingApp.Notification.Repositories;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Simple.WebAPI.Models;
using Simple.WebAPI.Models.Interfaces;

namespace Simple.WebAPI.Services
{
    public enum Action
    {
        Refresh,
        Deley
    }

    public enum Priority
    {
        Low,
        High,
        Logging,
    }

    public class NotificationService_old : BaseService, INotificationService_old
    {
        private CloudQueue _queue;

        public NotificationService_old()
        {
            // Retrieve storage account from connection string
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting(CONN_STR));

            // Create the queue client
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();

            // Retrieve a reference to a queue
            _queue = queueClient.GetQueueReference(TABLE_NOTIFICATION_QUEUE);

            // Create the queue if it doesn't already exist
            _queue.CreateIfNotExists();
        }


        public void Queue(dynamic data)
        {


            var message = data.Item.Message;

            // Create a message and add it to the queue.
            //CloudQueueMessage qmessage = new CloudQueueMessage(message);
            //_queue.AddMessage(qmessage);


            var queues = new NotificationRepository().GetNotifications();

            foreach (NotificationQueue notification in queues)
            {
                
                
            }

        }

        public void Push()
        {
          
            throw new System.NotImplementedException();
        }
    }
}