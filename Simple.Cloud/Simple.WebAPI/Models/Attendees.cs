﻿using Microsoft.WindowsAzure.Storage.Table;

namespace Simple.WebAPI.Models
{
    public class Attendee : TableEntity
    {
        public Attendee(string partitionKey, string rowKey)
        {
            this.PartitionKey = partitionKey;
            this.RowKey = rowKey;
        }

        public Attendee(){}

        public int Id { get; set; }
        public int MeetingId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public bool IsAttending { get; set; }
    }
}