﻿using System.Collections.Generic;

namespace Simple.WebAPI.Models.Repositories
{
    public interface IAttendeeRepository : IRepository<Attendee>
    {
        IEnumerable<Attendee> GetByMeetingId(int id);
    }
}