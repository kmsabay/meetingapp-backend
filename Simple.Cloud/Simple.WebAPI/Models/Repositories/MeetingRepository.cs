﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace Simple.WebAPI.Models.Repositories
{
    public class MeetingRepository : BaseRepository, IMeetingRepository
    {

        private CloudTable _table;

        private string _partitionKey ;


        public MeetingRepository()
        {
            
            _partitionKey = PARTITION_KEY;

            CloudStorageAccount storageAccount =
                CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting(CONN_STR));

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            _table = tableClient.GetTableReference(TABLE_MEETINGS);

            _table.CreateIfNotExists();

            GenerateSampleData();

        }

       


        public IEnumerable<Meeting> GetAll()
        {
            var query = new TableQuery<Meeting>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, _partitionKey));

            var results = _table.ExecuteQuery(query);


         

            return results.AsEnumerable();
        }

        public Meeting GetById(int id)
        {
            var query = new TableQuery<Meeting>()
                            .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, _partitionKey));

            var result = _table.ExecuteQuery(query).SingleOrDefault(r => r.Id == id);

            return result;
        }

        public int Create(Meeting meeting)
        {
            int newId = GenerateRandomId();

            var insertEntity = new Meeting(_partitionKey, GenerateRandomRowKey(newId));
            insertEntity.Id = newId;
            insertEntity.Organizer = meeting.Organizer;
            insertEntity.Title = meeting.Title;
            insertEntity.Location = meeting.Location;
            insertEntity.Schedule = meeting.Schedule;

            TableOperation insertOperation = TableOperation.Insert(insertEntity);

            _table.Execute(insertOperation);



          
            return 1;
        }

        public int Update(Meeting meeting)
        {
            TableOperation retrieveOperation = TableOperation.Retrieve<Meeting>(meeting.PartitionKey, meeting.RowKey);

            TableResult retrievedResult = _table.Execute(retrieveOperation);

            Meeting updateEntity = (Meeting) retrievedResult.Result;

            if (updateEntity != null)
            {
                updateEntity.Organizer = meeting.Organizer;
                updateEntity.Title = meeting.Title;
                updateEntity.Location = meeting.Location;
                updateEntity.Schedule = meeting.Schedule;

                TableOperation insertOrReplaceOperation = TableOperation.InsertOrReplace(updateEntity);

                _table.Execute(insertOrReplaceOperation);
            }


        
            return 1;
        }

        public int Delete(int id)
        {
            // Create a retrieve operation that expects a customer entity.
            TableOperation retrieveOperation = TableOperation.Retrieve<Meeting>(_partitionKey, id.ToString());

            // Execute the operation.
            TableResult retrievedResult = _table.Execute(retrieveOperation);

            // Assign the result to a CustomerEntity.
            Meeting deleteEntity = (Meeting) retrievedResult.Result;


            TableOperation deleteOperation = TableOperation.Delete(deleteEntity);

            // Execute the operation.
            _table.Execute(deleteOperation);

            return 1;
        }


        public Meeting GetMeetingById(int id)
        {
            var query = new TableQuery<Meeting>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, _partitionKey));

            var results = _table.ExecuteQuery(query);


            return results.SingleOrDefault(r => r.Id == id);
        }

        public  void GenerateSampleData()
        {
            if (GetAll().Any()) return;

            var batchOperation = new TableBatchOperation();

            var meeting1 = new Meeting(_partitionKey, "1")
                {
                    Id = 1,
                    Organizer = "Jane Citizen",
                    Title = "Workpac Targeted",
                    Schedule = DateTime.Now.AddMonths(1),
                    Location = "Sydney, NSW"
                };

            var meeting2 = new Meeting(_partitionKey, "2")
                {
                    Id = 2,
                    Organizer = "Giovanni Flores",
                    Title = "Test Meeting",
                    Schedule = DateTime.Now.AddMonths(2),
                    Location = "Bacoor, Cavite, Philippines"
                };

            var meeting3 = new Meeting(_partitionKey, "3")
                {
                    Id = 3,
                    Organizer = "Karlo Sabay",
                    Title = "Inuman Session",
                    Schedule = DateTime.Now.AddMonths(3),
                    Location = "Manila, Philippines"
                };

            var meeting4 = new Meeting(_partitionKey, "4")
                {
                    Id = 4,
                    Organizer = "Stuart Irvince",
                    Title = "Test Session",
                    Schedule = DateTime.Now.AddMonths(4),
                    Location = "Brisbane, QLD"
                };

            batchOperation.Insert(meeting1);
            batchOperation.Insert(meeting2);
            batchOperation.Insert(meeting3);
            batchOperation.Insert(meeting4);

            // Execute the batch operation.
            _table.ExecuteBatch(batchOperation);

        }




    }
}