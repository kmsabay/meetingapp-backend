﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace Simple.WebAPI.Models.Repositories
{
    public class AttendeeRepository : BaseRepository, IAttendeeRepository
    {

        private readonly CloudTable _table;
        private readonly string _partitionKey;

        public AttendeeRepository()
        {

            _partitionKey = PARTITION_KEY;

            CloudStorageAccount storageAccount =
                CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting(CONN_STR));

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            _table = tableClient.GetTableReference(TABLE_ATTENDEES);
            _table.CreateIfNotExists();

        }

        public IEnumerable<Attendee> GetAll()
        {

            var query = new TableQuery<Attendee>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, _partitionKey));

            var results = _table.ExecuteQuery(query);

            return results;
        }

        public Attendee GetById(int id)
        {

            var query = new TableQuery<Attendee>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, _partitionKey));

            var results = _table.ExecuteQuery(query).SingleOrDefault(r => r.Id == id);

            return results;
        }

        public int Create(Attendee attendies)
        {

            int newId = GenerateRandomId();

            var insertEntity = new Attendee(_partitionKey, GenerateRandomRowKey(newId));
            insertEntity.MeetingId = attendies.MeetingId;
            insertEntity.LastName = attendies.LastName;
            insertEntity.FirstName = attendies.FirstName;
            insertEntity.IsAttending = attendies.IsAttending;

            TableOperation insertOperation = TableOperation.Insert(insertEntity);

            _table.Execute(insertOperation);

            return 1;
        }

        public int Update(Attendee attendies)
        {
            
            // Create a retrieve operation that takes a customer entity.
            TableOperation retrieveOperation = TableOperation.Retrieve<Attendee>(attendies.PartitionKey,
                                                                                      attendies.RowKey);

            // Execute the operation.


            TableResult retrievedResult = _table.Execute(retrieveOperation);

            // Assign the result to a CustomerEntity object.
            var updateEntity = (Attendee)retrievedResult.Result;

            if (updateEntity != null)
            {
                // Change the phone number.
                updateEntity.MeetingId = attendies.MeetingId;
                updateEntity.LastName = attendies.LastName;
                updateEntity.FirstName = attendies.FirstName;
                updateEntity.IsAttending = attendies.IsAttending;

                // Create the InsertOrReplace TableOperation
                TableOperation insertOrReplaceOperation = TableOperation.InsertOrReplace(updateEntity);

                // Execute the operation.
                _table.Execute(insertOrReplaceOperation);
            }

            return 1;
        }

        public int Delete(int id)
        {
            // Create a retrieve operation that expects a customer entity.
            TableOperation retrieveOperation = TableOperation.Retrieve<Attendee>(_partitionKey, id.ToString());

            // Execute the operation.
            TableResult retrievedResult = _table.Execute(retrieveOperation);

            // Assign the result to a CustomerEntity.
            Attendee deleteEntity = (Attendee)retrievedResult.Result;


            TableOperation deleteOperation = TableOperation.Delete(deleteEntity);

            // Execute the operation.
            _table.Execute(deleteOperation);

            return 1;
        }

        public IEnumerable<Attendee> GetByMeetingId(int meetingId)
        {

            var query = new TableQuery<Attendee>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, _partitionKey));

            var results = _table.ExecuteQuery(query).Where(r => r.MeetingId == meetingId);

            return results;
        }

        public void GenerateSampleData()
        {

            // Create the batch operation.
            var batchOperation = new TableBatchOperation();

            // Create a customer entity and add it to the table.
            if (!GetAll().Any())
            {
                var attendant = new Attendee(_partitionKey, "1")
                    {
                        Id = 1,
                        MeetingId = 1,
                        LastName = "Dela Cruz",
                        FirstName = "Juan",
                        IsAttending = true
                    };

                batchOperation.Insert(attendant);

                attendant = new Attendee(_partitionKey, "2")
                    {
                        Id = 2,
                        MeetingId = 1,
                        LastName = "Aquino III",
                        FirstName = "Benigno",
                        IsAttending = false
                    };

                batchOperation.Insert(attendant);

                attendant = new Attendee(_partitionKey, "3")
                {
                    Id = 3,
                    MeetingId = 1,
                    LastName = "Binay",
                    FirstName = "Jejomar",
                    IsAttending = true
                };

                batchOperation.Insert(attendant);

                attendant = new Attendee(_partitionKey, "4")
                {
                    Id = 4,
                    MeetingId = 2,
                    LastName = "Kevin",
                    FirstName = "Garnett",
                    IsAttending = true
                };

                batchOperation.Insert(attendant);

                attendant = new Attendee(_partitionKey, "5")
                {
                    Id = 5,
                    MeetingId = 3,
                    LastName = "Kanor",
                    FirstName = "Mang",
                    IsAttending = false
                };

                batchOperation.Insert(attendant);
                

                // Execute the batch operation.
                _table.ExecuteBatch(batchOperation);
            }
        }

    }
}