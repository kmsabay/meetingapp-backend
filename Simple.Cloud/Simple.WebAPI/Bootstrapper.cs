using System.Web.Http;
using System.Web.Mvc;
using DinguBlue.MeetingApp.Common.Interfaces;
using DinguBlue.MeetingApp.Notification.Services;
using Microsoft.Practices.Unity;
using Simple.WebAPI.Areas.HelpPage.Controllers;
using Simple.WebAPI.Controllers;
using Simple.WebAPI.Models;
using Simple.WebAPI.Models.Repositories;
using Unity.Mvc4;


namespace Simple.WebAPI
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
      var container = BuildUnityContainer();

      DependencyResolver.SetResolver(new UnityDependencyResolver(container));

      //DependencyResolver.SetResolver(new UnityDependencyResolver(container));

      GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);

      return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();

      // register all your components with the container here
      // it is NOT necessary to register your controllers

      // e.g. container.RegisterType<ITestService, TestService>();    




      RegisterTypes(container);

      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
        // Register controller
        container.RegisterType<MeetingsController>();
        container.RegisterType<AttendeesController>();
        container.RegisterType<HelpController>();

        container.RegisterInstance<HttpConfiguration>(GlobalConfiguration.Configuration);

        container.RegisterType<IMeetingRepository, MeetingRepository>();
        container.RegisterType<IAttendeeRepository, AttendeeRepository>();

        container.RegisterType<INotificationService, NotificationService>();
        

    }
  }
}