﻿namespace DinguBlue.MeetingApp.Common.Enums
{
    public enum NotifyAction
    {
        Refresh,
        Deley
    }

    public enum PriorityLevel
    {
        Low =0,
        Medium=1,
        High=2,
        Highest = 9,
    }

}