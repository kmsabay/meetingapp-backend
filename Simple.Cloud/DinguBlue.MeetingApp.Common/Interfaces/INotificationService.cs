namespace DinguBlue.MeetingApp.Common.Interfaces
{
    public interface INotificationService
    {
        void Queue(dynamic notification);
        void Push();
    }
}