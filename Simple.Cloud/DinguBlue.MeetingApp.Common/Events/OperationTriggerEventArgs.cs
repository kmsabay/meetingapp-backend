﻿using System;
using System.Dynamic;

namespace DinguBlue.MeetingApp.Common.Events
{
    public delegate void OperationTriggeredHandler(object model, OperationTriggerEventArgs e);
  
    public class OperationTriggerEventArgs : EventArgs
    {
        private dynamic _item;

        public OperationTriggerEventArgs(ExpandoObject item)
        {
            _item = item;
        }

        public dynamic Item
        {
            get
            {
                return _item;
            }
        }
    }
}