﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="Simple.Cloud" generation="1" functional="0" release="0" Id="8353957f-5e5d-464f-b4d8-064bbdda3716" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="Simple.CloudGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="Simple.WebAPI:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/Simple.Cloud/Simple.CloudGroup/LB:Simple.WebAPI:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="Simple.WebAPI:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Simple.Cloud/Simple.CloudGroup/MapSimple.WebAPI:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="Simple.WebAPI:QueueStorageConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Simple.Cloud/Simple.CloudGroup/MapSimple.WebAPI:QueueStorageConnectionString" />
          </maps>
        </aCS>
        <aCS name="Simple.WebAPI:StorageConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Simple.Cloud/Simple.CloudGroup/MapSimple.WebAPI:StorageConnectionString" />
          </maps>
        </aCS>
        <aCS name="Simple.WebAPIInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/Simple.Cloud/Simple.CloudGroup/MapSimple.WebAPIInstances" />
          </maps>
        </aCS>
        <aCS name="Simple.Worker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Simple.Cloud/Simple.CloudGroup/MapSimple.Worker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="Simple.WorkerInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/Simple.Cloud/Simple.CloudGroup/MapSimple.WorkerInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:Simple.WebAPI:Endpoint1">
          <toPorts>
            <inPortMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.WebAPI/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapSimple.WebAPI:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.WebAPI/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapSimple.WebAPI:QueueStorageConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.WebAPI/QueueStorageConnectionString" />
          </setting>
        </map>
        <map name="MapSimple.WebAPI:StorageConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.WebAPI/StorageConnectionString" />
          </setting>
        </map>
        <map name="MapSimple.WebAPIInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.WebAPIInstances" />
          </setting>
        </map>
        <map name="MapSimple.Worker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.Worker/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapSimple.WorkerInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.WorkerInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="Simple.WebAPI" generation="1" functional="0" release="0" software="C:\Development\Research\AzureResearch\Simple.Cloud\csx\Debug\roles\Simple.WebAPI" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="1792" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="8080" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="QueueStorageConnectionString" defaultValue="" />
              <aCS name="StorageConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;Simple.WebAPI&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;Simple.WebAPI&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;Simple.Worker&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.WebAPIInstances" />
            <sCSPolicyUpdateDomainMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.WebAPIUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.WebAPIFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="Simple.Worker" generation="1" functional="0" release="0" software="C:\Development\Research\AzureResearch\Simple.Cloud\csx\Debug\roles\Simple.Worker" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="768" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;Simple.Worker&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;Simple.WebAPI&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;Simple.Worker&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.WorkerInstances" />
            <sCSPolicyUpdateDomainMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.WorkerUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.WorkerFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="Simple.WebAPIUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="Simple.WorkerUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="Simple.WebAPIFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="Simple.WorkerFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="Simple.WebAPIInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="Simple.WorkerInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="3a54532d-e443-483f-a206-a0d8e30b1fd4" ref="Microsoft.RedDog.Contract\ServiceContract\Simple.CloudContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="73dc6d2e-0d9e-42db-9afc-c20753abe05a" ref="Microsoft.RedDog.Contract\Interface\Simple.WebAPI:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/Simple.Cloud/Simple.CloudGroup/Simple.WebAPI:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>