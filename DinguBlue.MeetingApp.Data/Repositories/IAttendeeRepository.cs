﻿using System.Collections.Generic;


namespace DinguBlue.MeetingApp.Data.Repositories
{
    public interface IAttendeeRepository : IRepository<Attendee>
    {
        IEnumerable<Attendee> GetByMeetingId(int Id);
    }
}