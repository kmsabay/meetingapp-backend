using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace DinguBlue.MeetingApp.Data.Repositories
{
    public class Meeting : TableEntity
    {
        private string _organizer;
        private string _title;
        private DateTime _schedule;
        private string _location;
        private string _emailAddress;

        // declare a delegate for the bookpricechanged event
        public delegate void MeetingEntityChangedHandler(object sender, EntityChangedEventArgs e);
        
        // declare the bookpricechanged event using the bookpricechangeddelegate
        public event MeetingEntityChangedHandler OnMeetingEntityChanged;

        public Meeting(string partitionKey, string rowKey)
        {
            this.PartitionKey = partitionKey;
            this.RowKey = rowKey;
        }

        public Meeting() { }

        public int Id { get; set; }


        public string Organizer
        {
            get { return _organizer; }
            set
            {
                _organizer = value;
                
            }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value;

           
            }
        }

        public DateTime Schedule
        {
            get { return _schedule; }
            set { _schedule = value; }
        }

        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public string EmailAddress
        {
            get { return _emailAddress; }
            set { _emailAddress = value; }
        }


        
    }


    // specialized event class for the bookpricechanged event
    public class EntityChangedEventArgs : EventArgs
    {
        // instance variable to store the book price
        private object _item;

        // constructor that sets book price
        public EntityChangedEventArgs(object item )
        {
         
            
            _item = item;
        }

        // public property for the book price
        public object Item
        {
            get { return _item; }
        }
    }
}