﻿using DinguBlue.MeetingApp.Data.Repositories.Interfaces;

namespace DinguBlue.MeetingApp.Data.Repositories
{
    public interface IMeetingRepository : IRepository<Meeting>
    {
        
        
    }
}