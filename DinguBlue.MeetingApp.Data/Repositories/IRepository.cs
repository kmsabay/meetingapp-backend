﻿using System.Collections.Generic;

namespace DinguBlue.MeetingApp.Data.Repositories
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        int Create(T item);
        int Update(T item);
        int Delete(int id);
    }
}