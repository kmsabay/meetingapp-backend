﻿using System.Collections.Generic;

namespace DinguBlue.MeetingApp.Notification.Repositories
{
    public interface INotificationRepository<T>
    {
        IEnumerable<T> GetAllNotificationsQueue();
        void SaveNotificationQueue(T notificationQueue);
    }
}