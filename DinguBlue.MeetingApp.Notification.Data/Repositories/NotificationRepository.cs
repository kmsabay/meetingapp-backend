﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DinguBlue.MeetingApp.Notification.Repositories
{


    public class NotificationRepository : INotificationRepository<NotificationQueue>
    {
        private readonly NotificationDBEntities _context;

        //public NotificationRepository(DbContext context)
        public NotificationRepository(DbContext context = null)
        {
            if(context == null) 
                _context = new NotificationDBEntities();   
        }


        public IEnumerable<NotificationQueue> GetAllNotificationsQueue()
        {
            using (_context)
            {
                var notificationQuery = from n in _context.NotificationQueues select n;

                return notificationQuery.ToList();
            }
        }

        public void SaveNotificationQueue(NotificationQueue notificationQueue)
        {
            using (_context)
            {
                _context.NotificationQueues.Add(notificationQueue);
                _context.SaveChanges();
            }
        }
    }

}