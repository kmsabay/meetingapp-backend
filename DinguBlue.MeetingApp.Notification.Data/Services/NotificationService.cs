﻿using System;
using DinguBlue.MeetingApp.Common.Interfaces;
using DinguBlue.MeetingApp.Notification.Repositories;

namespace DinguBlue.MeetingApp.Notification.Services
{
    public class NotificationService : BaseService, INotificationService
    {
        private readonly NotificationRepository _repository;
        public NotificationService()
        {

            _repository = new NotificationRepository();
        }
        
        public void Queue(dynamic data)
        {
            var notify = data.Item;

            var notificationQueue = new NotificationQueue
                {
                    SenderId = notify.SenderId,
                    Message = notify.Message,
                    Action = Convert.ToString(notify.Action),
                    Entities = notify.Entities,
                    PriorityLevel = (int)notify.PriorityLevel
                };

            _repository.SaveNotificationQueue(notificationQueue);
        }

        public void Push()
        {
          
            throw new System.NotImplementedException();
        }
    }
}
