﻿namespace DinguBlue.MeetingApp.Notification.Services
{
    public class BaseService
    {
        protected const string CONN_STR = "QueueStorageConnectionString";

        protected const string TABLE_NOTIFICATION_QUEUE = "notificationqueue"; 
    }
}